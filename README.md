# BinPackingAPI
Simple Bin Packing API, how many containers we need, to fit certain amount of items

to build project, we need the following dependencies:
 -Java 1.8+
 -Maven


To simply install it as a service, you need to install JDK, or at least JRE,
  and configure java bin folder in SYSTEM PATH environment variable.

Open a terminal with admin permissions in the root of the project.
  and exec

>> BinPackingAPI.exe install
>> BinPackingAPI.exe start
