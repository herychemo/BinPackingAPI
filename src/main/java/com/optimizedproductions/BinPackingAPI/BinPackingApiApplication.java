package com.optimizedproductions.BinPackingAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BinPackingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BinPackingApiApplication.class, args);
	}
}
