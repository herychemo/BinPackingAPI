package com.optimizedproductions.BinPackingAPI.controllers;

import com.optimizedproductions.BinPackingAPI.dto.InputDTO;
import com.optimizedproductions.BinPackingAPI.dto.OutputDTO;
import com.optimizedproductions.BinPackingAPI.services.BinPackingService;
import com.optimizedproductions.BinPackingAPI.utils.ErrorCodes;

public class BinPackingController {

    public static OutputDTO whatDoWeNeed(BinPackingService packingService, InputDTO inputDTO){
        OutputDTO outputDTO = new OutputDTO();

        if (inputDTO.availableContainers == null || inputDTO.availableContainers.size() == 0
                || inputDTO.items == null || inputDTO.items.size() == 0) {
            outputDTO.errorCode = ErrorCodes.ERROR_INCOMPLETE_INPUT_DATA;
        }else{
            outputDTO.resultContainers = packingService.whatDoWeNeedV2(inputDTO.availableContainers, inputDTO.items);

            if (outputDTO.resultContainers == null)
                outputDTO.errorCode = ErrorCodes.ERROR_SOLUTION_NOT_FOUND;
            else
                outputDTO.errorCode = ErrorCodes.NO_ERROR;
        }

        return outputDTO;
    }

}
