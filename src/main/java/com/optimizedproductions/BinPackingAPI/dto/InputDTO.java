package com.optimizedproductions.BinPackingAPI.dto;

import com.optimizedproductions.BinPackingAPI.entities.SimpleContainer;
import com.optimizedproductions.BinPackingAPI.entities.SimpleItem;

import java.util.List;

public class InputDTO {

    public List<SimpleContainer> availableContainers;
    public List<SimpleItem> items;

}
