package com.optimizedproductions.BinPackingAPI.dto;

import com.optimizedproductions.BinPackingAPI.entities.FillingContainer;

import java.util.List;

public class OutputDTO {

    public List<FillingContainer> resultContainers;
    public int errorCode;

}
