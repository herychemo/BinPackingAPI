package com.optimizedproductions.BinPackingAPI.entities;

import java.util.List;

public class FillingContainer extends SimpleContainer {

    private List<SimpleItem> items;

    public FillingContainer() {
        super();
    }

    public FillingContainer(int width, int height, int depth) {
        super(width, height, depth);
    }

    public FillingContainer(SimpleContainer container, List<SimpleItem> items ){
        super(container.getWidth(), container.getHeight(), container.getDepth(),
                container.getMaxWeight(), container.getName(), container.getId());
        this.items = items;
    }

    public List<SimpleItem> getItems() {
        return items;
    }

    public void setItems(List<SimpleItem> items) {
        this.items = items;
    }

    public int countItems(){
        return items.size();
    }

    public long getUsedVolume(){
        return items.stream().mapToLong(SimpleItem::getVolumen).sum();
    }

    public long getFreeVolume(){
        return getVolumen() - getUsedVolume();
    }

    public long getUsedWeight() {
        return items.stream().mapToLong(SimpleItem::getWeight).sum();
    }
    public long getFreeWeight() {
        return getMaxWeight() - getUsedWeight();
    }

}
