package com.optimizedproductions.BinPackingAPI.entities;

public class SimpleContainer extends SimpleItem {

    private int maxWeight;

    public SimpleContainer() {
        super();
    }

    public SimpleContainer(int width, int height, int depth, String name) {
        super(width, height, depth, 0, name);
    }

    public SimpleContainer(int width, int height, int depth) {
        super(width, height, depth, 0);
    }

    public SimpleContainer(int width, int height, int depth, int maxWeight) {
        super(width, height, depth, 0);
        this.maxWeight = maxWeight;
    }

    public SimpleContainer(int width, int height, int depth, int maxWeight, String name) {
        super(width, height, depth, 0, name);
        this.maxWeight = maxWeight;
    }

    public SimpleContainer(int width, int height, int depth, int maxWeight, String name, String id) {
        super(id, width, height, depth, 0, name);
        this.maxWeight = maxWeight;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

}
