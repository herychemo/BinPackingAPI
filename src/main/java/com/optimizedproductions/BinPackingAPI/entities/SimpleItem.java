package com.optimizedproductions.BinPackingAPI.entities;

import java.util.Arrays;
import java.util.Collections;

public class SimpleItem {

    private String id;
    private int width;
    private int height;
    private int depth;
    private int weight;
    private String name;

    public SimpleItem(String id, int width, int height, int depth, int weight, String name) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.weight = weight;
        this.name = name;
    }

    public SimpleItem(int width, int height, int depth, int weight, String name) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.weight = weight;
        this.name = name;
    }

    public SimpleItem(int width, int height, int depth, int weight) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.weight = weight;
    }

    public SimpleItem(int width, int height, int depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public SimpleItem() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getVolumen(){
        return height * width * depth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxDimension(){
        return Collections.max(  Arrays.asList(getWidth(), getHeight(), getDepth()));
    }

}
