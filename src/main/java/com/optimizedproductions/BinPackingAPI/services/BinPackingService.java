package com.optimizedproductions.BinPackingAPI.services;

import com.github.skjolberg.packing.*;
import com.optimizedproductions.BinPackingAPI.entities.FillingContainer;
import com.optimizedproductions.BinPackingAPI.entities.SimpleContainer;
import com.optimizedproductions.BinPackingAPI.entities.SimpleItem;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BinPackingService {

    // Sample method, but using our custom simple objects (serializable entities into json responses )
    public List<FillingContainer> whatDoWeNeedV2(List<SimpleContainer> availableContainers, List<SimpleItem> items){
        items.sort(Comparator.comparing(SimpleItem::getMaxDimension));     // Order items based on its largest property (asc)
        Collections.reverse(items);  // reverse item's list (desc)

        //create some util variables
        List<Pair<FillingContainer, Integer>> neededContainers = new ArrayList<>();
        List<SimpleItem> currentFit = new ArrayList<>();
        int currentContainer = 0;
        FillingContainer workingCopy = null;

        for (int idx = 0 ; idx < items.size();) {    // Iterate over item's list
            SimpleItem item = items.get(idx);

            // before all, check if item can fit in one of the existing solutions
            boolean legacyFit = false;      // iterate over previous solutions
            for (int j = 0; j < neededContainers.size() ;j++) {
                Pair<FillingContainer, Integer> workingFit = neededContainers.get(j);
                if ( workingFit.getValue0().getFreeVolume() < item.getVolumen() )
                    continue;   // If the solution free volume is not big enough, don't spend time
                if ( workingFit.getValue0().getFreeWeight() < item.getWeight() )
                    continue;   // If the solution free weight is not big enough, don't spend time

                workingFit.getValue0().getItems().add(item);

                FillingContainer internalFit = testConfiguration(availableContainers.get(workingFit.getValue1()), workingFit.getValue0().getItems());
                if (internalFit != null){
                    neededContainers.remove(workingFit);
                    neededContainers.add(
                            workingFit.setAt0(internalFit)
                    );
                    legacyFit = true;
                    break;
                }else
                    workingFit.getValue0().getItems().remove(item);
            }

            if (legacyFit){
                idx++;
                continue;
            }

            currentFit.add(item);
            FillingContainer fit = testConfiguration(availableContainers.get(currentContainer), currentFit);

            if (fit != null){
                workingCopy = fit;
            }else{
                currentFit.remove(item);

                if (workingCopy != null){
                    neededContainers.add(new Pair<>(workingCopy, currentContainer));
                    workingCopy = null;
                    currentContainer = 0;
                    currentFit.clear();
                }else {
                    currentContainer++;
                    if (currentContainer >= availableContainers.size())
                        return null;
                }
                continue;
            }
            idx++;
        }

        if (workingCopy != null)
            neededContainers.add(new Pair<>(workingCopy, currentContainer));

        List<FillingContainer> result = new ArrayList<>();
        neededContainers.forEach(objects -> result.add(objects.getValue0()));
        return result;
    }

    public List<Container> whatDoWeNeed(List<Dimension> availableContainers, List<BoxItem> products) {
        products.sort((o1, o2) -> {     // Order items based on its largest property (asc)
            Integer v1 = Collections.max(  Arrays.asList(o1.getBox().getWidth(),o1.getBox().getHeight(), o1.getBox().getDepth()));
            Integer v2 = Collections.max(  Arrays.asList(o2.getBox().getWidth(),o2.getBox().getHeight(), o2.getBox().getDepth()));
            return v1.compareTo(v2);
        });
        Collections.reverse(products);  // reverse item's list (desc)

        List<Triplet<Container, List<BoxItem>, Integer>> neededContainers = new ArrayList<>();
        List<BoxItem> currentFit = new ArrayList<>();
        int currentContainer = 0;
        Container workingCopy = null;

        for (int idx = 0 ; idx < products.size();) {    // Iterate over item's list
            BoxItem item = products.get(idx);

            // before all, check if item can fit in one of the existing solutions
            boolean legacyFit = false;      // iterate over previous solutions
            for (int j = 0; j < neededContainers.size() ;j++) {
                Triplet<Container, List<BoxItem>, Integer> workingFit = neededContainers.get(j);
                long fV1 = workingFit.getValue0().getVolume() - workingFit.getValue0().getLevels().stream().mapToLong(lvl -> lvl.stream().mapToLong(itm -> itm.getBox().getVolume()).sum()).sum();
                long fV2 = item.getBox().getVolume();
                if ( fV1 < fV2 )
                    continue;   // If the solution free volume is not big enough, don't spend time
                workingFit.getValue1().add(item);
                Container internalFit = testConfiguration(availableContainers.get(workingFit.getValue2()), workingFit.getValue1());
                if (internalFit != null){
                    neededContainers.remove(workingFit);
                    neededContainers.add(
                            workingFit.setAt0(internalFit)
                    );
                    legacyFit = true;
                    break;
                }else
                    workingFit.getValue1().remove(item);
            }

            if (legacyFit){
                idx++;
                continue;
            }

            currentFit.add(item);
            Container fit = testConfiguration(availableContainers.get(currentContainer), currentFit);

            if (fit != null){
                workingCopy = fit;
            }else{
                currentFit.remove(item);

                if (workingCopy != null){
                    List<BoxItem> cFitcopy = new ArrayList<>(currentFit);
                    neededContainers.add(new Triplet<>(workingCopy, cFitcopy, currentContainer));
                    workingCopy = null;
                    currentContainer = 0;
                    currentFit.clear();
                }else {
                    currentContainer++;
                    if (currentContainer >= availableContainers.size())
                        return null;
                }
                continue;
            }
            idx++;
        }
        if (workingCopy != null)
            neededContainers.add(new Triplet<>(workingCopy, currentFit, currentContainer));

        List<Container> result = new ArrayList<>();
        neededContainers.forEach(objects -> result.add(objects.getValue0()));
        return result;
    }

    private FillingContainer testConfiguration(SimpleContainer container, List<SimpleItem> currentList) {
        Dimension dimension = new Dimension(container.getWidth(), container.getDepth(), container.getHeight());

        List<BoxItem> curList = currentList.stream().map(
                simpleItem -> new BoxItem(new Box(simpleItem.getWidth(), simpleItem.getDepth(), simpleItem.getHeight()))
        ).collect(Collectors.toList());

        Container match = testConfiguration(dimension, curList);
        if (match != null)
            return new FillingContainer(container, new ArrayList<>(currentList));
        return null;
    }

    private Container testConfiguration(Dimension container, List<BoxItem> currentList ) {
        List<Dimension> containerList = new ArrayList<>();
        containerList.add(container);
        Packager packager = new LargestAreaFitFirstPackager(containerList);
        return packager.pack(currentList);
    }

}
