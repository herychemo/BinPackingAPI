package com.optimizedproductions.BinPackingAPI.utils;

public class ErrorCodes {

    public static final int NO_ERROR = 0;
    public static final int ERROR_INCOMPLETE_INPUT_DATA = 1;

    public static final int ERROR_SOLUTION_NOT_FOUND = 2;
}
