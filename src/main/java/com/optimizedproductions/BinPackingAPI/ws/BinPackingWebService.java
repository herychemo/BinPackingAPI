package com.optimizedproductions.BinPackingAPI.ws;

import com.optimizedproductions.BinPackingAPI.controllers.BinPackingController;
import com.optimizedproductions.BinPackingAPI.dto.InputDTO;
import com.optimizedproductions.BinPackingAPI.dto.OutputDTO;
import com.optimizedproductions.BinPackingAPI.services.BinPackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/ws")
@RestController
public class BinPackingWebService {

    @Autowired
    private BinPackingService packingService;

    @PostMapping("/process")
    public OutputDTO processSet(@RequestBody InputDTO dto){
        return BinPackingController.whatDoWeNeed(packingService, dto);
    }

}
