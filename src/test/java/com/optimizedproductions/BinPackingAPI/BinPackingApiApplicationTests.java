package com.optimizedproductions.BinPackingAPI;

import com.github.skjolberg.packing.Box;
import com.github.skjolberg.packing.BoxItem;
import com.github.skjolberg.packing.Container;
import com.github.skjolberg.packing.Dimension;
import com.optimizedproductions.BinPackingAPI.services.BinPackingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ContextConfiguration(classes = {BinPackingApiApplication.class})
public class BinPackingApiApplicationTests {

	@Autowired
	private BinPackingService packingService;

	@Test
	public void contextLoads() {
        assert packingService != null;
	}

    @Test
    public void simpleAndLargeTest() {
        long timeStart = new Date().getTime();
        System.out.println(timeStart);

        List<Dimension> containers = getBoxes();
        List<BoxItem> products = prepareItems(3, 3, 0);

        List<Container> matches = packingService.whatDoWeNeed(containers, products);

        assert matches.size() == 2; // Cantidad de cajas

        Container box1 = matches.get(0),    box2 = matches.get(1);

        int items4box1 = box1.getLevels().stream().mapToInt(ArrayList::size).sum();
        int items4box2 = box2.getLevels().stream().mapToInt(ArrayList::size).sum();

        assert items4box1 == 5;   // Items en la primer caja
        assert items4box2 == 1;   // Items en segunda caja

        long timeEnd = new Date().getTime();
        System.out.println(timeEnd);
        System.out.println(String.format("It spent %d mili seconds", timeEnd - timeStart));
    }

    @Test
    public void simpleLargeAndFatTest() {
        long timeStart = new Date().getTime();
        System.out.println(timeStart);

        List<Dimension> containers = getBoxes();
        List<BoxItem> products = prepareItems(3, 3, 1);

        List<Container> matches = packingService.whatDoWeNeed(containers, products);

        assert matches.size() == 2; // Cantidad de cajas

        Container box1 = matches.get(0),    box2 = matches.get(1);

        int items4box1 = box1.getLevels().stream().mapToInt(ArrayList::size).sum();
        int items4box2 = box2.getLevels().stream().mapToInt(ArrayList::size).sum();

        assert items4box1 == 5;   // Items en la primer caja
        assert items4box2 == 2;   // Items en segunda caja

        long timeEnd = new Date().getTime();
        System.out.println(timeEnd);
        System.out.println(String.format("It spent %d mili seconds", timeEnd - timeStart));
    }

    private List<BoxItem> prepareItems(int simple_items,int large_items, int fat_items ){
        List<BoxItem> products = new ArrayList<>();
        for (int i=0;i<simple_items;i++)
            products.add(new BoxItem(new Box("simple", 10, 10, 100), 1));
        for (int i=0;i<large_items;i++)
            products.add(new BoxItem(new Box("large", 10, 10, 200), 1));
        for (int i=0;i<fat_items;i++)
            products.add(new BoxItem(new Box("fat", 13, 13, 100), 1));
        return products;
    }

    private List<Dimension> getBoxes() {
        Dimension boxTypeA = new Dimension("A", 30, 100, 40);
        Dimension boxTypeB = new Dimension("B", 20, 200, 20);
        List<Dimension> containers = new ArrayList<>();
        containers.add(boxTypeA);
        containers.add(boxTypeB);
        return containers;
    }


}
