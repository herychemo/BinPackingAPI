package com.optimizedproductions.BinPackingAPI;


import com.optimizedproductions.BinPackingAPI.entities.FillingContainer;
import com.optimizedproductions.BinPackingAPI.entities.SimpleContainer;
import com.optimizedproductions.BinPackingAPI.entities.SimpleItem;
import com.optimizedproductions.BinPackingAPI.services.BinPackingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ContextConfiguration(classes = {BinPackingApiApplication.class})
public class BinPackingApiV2ApplicationTests {

    @Autowired
    private BinPackingService packingService;

    @Test
    public void contextLoads() {
        assert packingService != null;
    }

    @Test
    public void simpleAndLargeTest() {
        long timeStart = new Date().getTime();
        System.out.println(timeStart);

        List<SimpleContainer> containers = getBoxes();
        List<SimpleItem> products = prepareItems(3, 3, 0);

        List<FillingContainer> matches = packingService.whatDoWeNeedV2(containers, products);

        assert matches.size() == 2; // Cantidad de cajas

        FillingContainer box1 = matches.get(0),    box2 = matches.get(1);

        assert box1.getItems().size() == 5;   // Items en la primer caja
        assert box2.getItems().size() == 1;   // Items en segunda caja

        long timeEnd = new Date().getTime();
        System.out.println(timeEnd);
        System.out.println(String.format("It spent %d mili seconds", timeEnd - timeStart));
    }

    @Test
    public void simpleLargeAndFatTest() {
        long timeStart = new Date().getTime();
        System.out.println(timeStart);

        List<SimpleContainer> containers = getBoxes();
        List<SimpleItem> products = prepareItems(3, 3, 1);

        List<FillingContainer> matches = packingService.whatDoWeNeedV2(containers, products);

        assert matches.size() == 2; // Cantidad de cajas

        FillingContainer box1 = matches.get(0),    box2 = matches.get(1);

        assert box1.getItems().size() == 5;   // Items en la primer caja
        assert box2.getItems().size() == 2;   // Items en segunda caja

        long timeEnd = new Date().getTime();
        System.out.println(timeEnd);
        System.out.println(String.format("It spent %d mili seconds", timeEnd - timeStart));
    }

    private List<SimpleItem> prepareItems(int simple_items,int large_items, int fat_items ){
        List<SimpleItem> products = new ArrayList<>();
        for (int i=0;i<simple_items;i++)
            products.add(new SimpleItem(10, 10, 100, 40, "simple"));
        for (int i=0;i<large_items;i++)
            products.add(new SimpleItem(10, 10, 200, 80, "large"));
        for (int i=0;i<fat_items;i++)
            products.add(new SimpleItem(13, 13, 100, 70, "fat"));
        return products;
    }

    private List<SimpleContainer> getBoxes() {
        SimpleContainer boxTypeA = new SimpleContainer(30, 40, 100, 500, "A" );
        SimpleContainer boxTypeB = new SimpleContainer(20, 20, 200, 500, "B" );

        List<SimpleContainer> containers = new ArrayList<>();
        containers.add(boxTypeA);
        containers.add(boxTypeB);
        return containers;
    }

}
